const mapOriginal = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

const map = mapOriginal.map(row => [...row])

let ladyBugElement = document.createElement('div')
ladyBugElement.className = 'ladybug'

function buildMaze(map) {
    const mazeElem = document.getElementById("maze")
    for (i = 0; i < map.length; i++) {
        const currentString = map[i]
        const columnDiv = document.createElement('div')
        columnDiv.classList.add('column')
        for (j = 0; j < currentString.length; j++) {
            const currentCharacter = currentString[j]
            let newDiv = document.createElement('div')
            newDiv.dataset.columnIndex = j
            newDiv.dataset.rowIndex = i
            switch (currentCharacter) {
                case 'W':
                    newDiv.className = 'wall'
                    newDiv.dataset.cellType = 'wall'
                    break
                case ' ':
                    newDiv.className = 'space'
                    newDiv.dataset.cellType = 'space'
                    break
                case 'S':
                    newDiv.className = 'start'
                    newDiv.dataset.cellType = 'start'
                    ladyBugElement.dataset.ladyColumn = parseInt(0)
                    ladyBugElement.dataset.ladyRow = parseInt(9)
                    newDiv.appendChild(ladyBugElement)
                    break
                case 'F':
                    newDiv.className = 'finish'
                    newDiv.dataset.cellType = 'finish'
                    break

            }
            columnDiv.appendChild(newDiv)
        }
        mazeElem.appendChild(columnDiv)
    }
}
buildMaze(map)

//get start element position
function getPlayerStart() {
    let startDiv = document.querySelector('.ladybug')
    let playerStyle = window.getComputedStyle(startDiv)
    let playerTop = playerStyle.getPropertyValue('top')
    let playerLeft = playerStyle.getPropertyValue('left')
    playerTop = parseInt(playerTop)
    playerLeft = parseInt(playerLeft)
    return [playerTop, playerLeft]
}

bugTop = getPlayerStart()[0]
bugLeft = getPlayerStart()[1]


document.addEventListener('keydown', function keyHandler(event) {
    event.preventDefault()

    let downPos = map[Number(ladyBugElement.dataset.ladyRow) + 1][Number(ladyBugElement.dataset.ladyColumn)]
    let upPos = map[Number(ladyBugElement.dataset.ladyRow) - 1][Number(ladyBugElement.dataset.ladyColumn)]
    let leftPos = map[Number(ladyBugElement.dataset.ladyRow)][Number(ladyBugElement.dataset.ladyColumn) - 1]
    let rightPos = map[Number(ladyBugElement.dataset.ladyRow)][Number(ladyBugElement.dataset.ladyColumn) + 1]
    
    let currentPos = map[Number(ladyBugElement.dataset.ladyRow)][Number(ladyBugElement.dataset.ladyColumn)]
    
    if(currentPos === 'F') {
        alert('You won!')
        document.removeEventListener('keydown', keyHandler)
    } else if (event.key === 'ArrowDown') {

        if (downPos != 'W') {
            bugTop += 10
            ladyBugElement.dataset.ladyRow = Number(ladyBugElement.dataset.ladyRow) + 1
            ladyBugElement.style.top = bugTop + "px"
        }
    }
    else if (event.key === 'ArrowUp') {
        if (upPos != 'W') {
            bugTop -= 10
            ladyBugElement.dataset.ladyRow = Number(ladyBugElement.dataset.ladyRow) - 1
            ladyBugElement.style.top = bugTop + "px"
        }
    }
    else if (event.key === 'ArrowLeft' && currentPos != 'S') {
        if (leftPos != 'W') {
            bugLeft -= 10
            ladyBugElement.dataset.ladyColumn = Number(ladyBugElement.dataset.ladyColumn) - 1
            ladyBugElement.style.left = bugLeft + "px"
        }
    }
    else if (event.key === 'ArrowRight') {
        if (rightPos != 'W') {
            bugLeft += 10
            ladyBugElement.dataset.ladyColumn = Number(ladyBugElement.dataset.ladyColumn) + 1
            ladyBugElement.style.left = bugLeft + "px"
        }
    }
    
})